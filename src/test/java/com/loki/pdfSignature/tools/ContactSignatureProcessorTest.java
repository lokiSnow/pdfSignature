package com.loki.pdfSignature.tools;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;
import org.springframework.util.ResourceUtils;

public class ContactSignatureProcessorTest {

	@Test
	public void testPerson() throws Exception {
		byte[] result = new ContactSignatureProcessor(prepareContactContextPerson()).process();
		FileOutputStream out = new FileOutputStream("E://person.pdf");
		out.write(result);
		out.close();
	}
	@Test
	public void testOrga() throws Exception {
		byte[] result = new ContactSignatureProcessor(prepareContactContextOrga()).process();
		FileOutputStream out = new FileOutputStream("E://orga.pdf");
		out.write(result);
		out.close();
	}

	public static ContactSignatureContext prepareContactContextPerson() throws Exception {
		ContactSignatureContext context = new ContactSignatureContext();
		context.setCustomerId(999L);
		context.setCustomerType(CustomerType.PERSON.getCode());
		context.setCustomerName("我名loki");
		Map<String,String> certs = new HashMap<>();
		certs.put(CertType.ID_CARD.getName(), "18900000001");
		context.setCerts(certs);
		context.setEmail("loki@163.com");
		context.setAddress("宇宙");
		context.setBankAccountName("呵呵");
		context.setBankNo("8888888888888888");
		context.setDepositBank("宇宙行");
		context.setAppointAmount("99999999999");
		context.setAppointAmountCN("前面看上去只能写10位了");
		context.setOtherFee("0");
		context.setOtherFeeCN("不收费");
		context.setCustomerSignature(readFile("loki.png"));
		context.setContact(readFile("pages.pdf"));
		context.setYear(String.valueOf(new Date().getYear()));
		context.setMonth(String.valueOf(new Date().getMonth()));
		context.setDay(String.valueOf(new Date().getDay()));
		context.setSignReason("测试个人客户PDF签署");
		return context;
	}
	
	public static ContactSignatureContext prepareContactContextOrga() throws Exception {
		ContactSignatureContext context = new ContactSignatureContext();
		context.setCustomerId(999L);
		context.setCustomerType(CustomerType.ORGA.getCode());
		context.setOrgaName("一个机构");
		Map<String,String> certs = new HashMap<>();
		certs.put(CertType.SOCIAL_CREDIT_CODE.getName(), "111111");
		certs.put(CertType.ORGA_CODE.getName(), "222222");
		context.setCerts(certs);
		context.setLegalName("都是我");
		context.setEmail("orga@163.com");
		context.setAddress("不知道");
		context.setBankAccountName("测测测测测测测");
		context.setBankNo("6666666666666666");
		context.setDepositBank("未知行");
		context.setAppointAmount("5555555555");
		context.setAppointAmountCN("前面看上去只能写10位了");
		context.setOtherFee("0");
		context.setOtherFeeCN("不收费");
		context.setLegalSignature(readFile("loki.png"));
		context.setLegalStamp(readFile("王瑞雪印.gif"));
		context.setContact(readFile("pages.pdf"));
		context.setYear(String.valueOf(new Date().getYear()));
		context.setMonth(String.valueOf(new Date().getMonth()));
		context.setDay(String.valueOf(new Date().getDay()));
		context.setSignReason("测试机构客户PDF签署");
		return context;
	}
	
	public static byte[] readFile(String fileName) throws Exception{
		File file = ResourceUtils.getFile(ResourceUtils.CLASSPATH_URL_PREFIX+fileName);
		byte[] content = new byte[(int) file.length()];
		FileInputStream in = new FileInputStream(file);
		in.read(content);
		in.close();
		return content;
	}
}
