/**
 * 
 */
package com.loki.pdfSignature.tools;

import java.util.Map;

import lombok.Data;

/**
 * @author loki
 *
 */
@Data
public class ContactSignatureContext {

	/** 客户ID **/
	private Long customerId;
	/** 客户类型 **/
	private Integer customerType;
	/** 个人客户姓名 **/
	private String customerName;
	/** 客户证件(type:certNo,type use CertType enum names like 'ID_CARD') **/
	private Map<String,String> certs;
	/** 客户邮箱 **/
	private String email;
	/** 客户地址 **/
	private String address;
	/** 机构客户名称 **/
	private String orgaName;
	/** 机构代表人姓名 **/
	private String legalName;
	/** 投资账号姓名 **/
	private String bankAccountName;
	/** 投资账号 **/
	private String bankNo;
	/** 投资账号开户行 **/
	private String depositBank;
	/** 认、申购金额 **/
	private String appointAmount;
	/** 认、申购金额大写 **/
	private String appointAmountCN;
	/** 认、申购其他费用 **/
	private String otherFee;
	/** 认、申购其他费用大写 **/
	private String otherFeeCN;
	/** 客户签名图片 **/
	private byte[] customerSignature;
	/** 机构签名图片 **/
	private byte[] legalSignature;
	/** 机构印章图片 **/
	private byte[] legalStamp;
	/** 待签署合同 **/
	private byte[] contact;
	/** 签署时间-年 **/
	private String year;
	/** 签署时间-月 **/
	private String month;
	/** 签署时间-日 **/
	private String day;
	/** 签署原因-日 **/
	private String signReason;
	
}
