/**
 * 
 */
package com.loki.pdfSignature.tools;

import java.util.HashMap;
import java.util.Map;

/**
 * @author loki
 *
 */
public enum CertType {
	ID_CARD(1,"身份证"),
	PASSPORT(2,"护照"),
	OFFICER(3,"军官证"),
	TAIWAN(4,"台胞证"),
	HK_MACAO(5,"港澳通行证"),
	SOCIAL_CREDIT_CODE(6,"统一社会信用代码"),
	BUSINESS_LICENSE(7,"营业执照注册号"),
	TAX_CERT(8,"税务登记证号码"),
	ORGA_CODE(9,"组织机构代码"),
	PERMANENT_RESIDENT_PERMIT(20,"永久居留证"),
	OTHER(99,"其他"),
	 ;
	
	public static final Map<String,CertType> ORGA = new HashMap<String,CertType>(){{
		put("SOCIAL_CREDIT_CODE", SOCIAL_CREDIT_CODE);
		put("BUSINESS_LICENSE", BUSINESS_LICENSE);
		put("TAX_CERT", TAX_CERT);
		put("ORGA_CODE", ORGA_CODE);
	}};

	 private Integer code;

	 private String desc;

	 private CertType(Integer code, String desc) {
	 this.code = code;
	 this.desc = desc;
	 }
	 
	 /**
	 * 根据枚举代码获取枚举信息
	 *
	 * @param code
     * @return
     */
    public static CertType get(Integer code) {
        for (CertType certType : CertType.values()) {
            if (certType.getCode().equals(code)) {
                return certType;
            }
        }

        return null;
    }
    
    public static CertType getByName(String name) {
        for (CertType certType : CertType.values()) {
            if (certType.getName().equals(name)) {
                return certType;
            }
        }

        return null;
    }
	
	public Integer getCode() {
		return code;
	}
	
	public String getDesc() {
		return desc;
	}
	
	public String getName() {
		return this.name();
	}
}
