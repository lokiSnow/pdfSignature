/**
 * 
 */
package com.loki.pdfSignature.tools;

/**
 * @author hh
 *
 */
public enum CustomerType {
	PERSON(1,"个人客户"),
	ORGA(2,"机构客户");
	
	private Integer code;

	private String desc;

	private CustomerType(Integer code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	 /**
	 * 根据枚举代码获取枚举信息
	 *
	 * @param code
    * @return
    */
    public static CustomerType get(Integer code) {
       for (CustomerType type : CustomerType.values()) {
           if (type.getCode().equals(code)) {
               return type;
           }
       }

       return null;
    }
	
	public Integer getCode() {
		return code;
	}
	
	public String getDesc() {
		return desc;
	}
	
	public String getName() {
		return this.name();
	}
}
