/**
 * 
 */
package com.loki.pdfSignature.tools;

import java.util.Properties;

import lombok.Data;

/**
 * @author loki
 *
 */
@Data
public class CertificateInfo {

	private String Id;
	/** 所属用户ID **/
	private Long owner;
	/** 证书编码，can be alias of a cert entry in a JKS **/
	private String code;
	/** 是否有效 **/
	private Boolean state;
	/** 证书类型，{@code CertificateType} enum **/
	private Integer type;
	/** 证书内容 **/
	private String content;
	/** 证书其他扩展属性 **/
	private Properties properties = new Properties();
	
}
